# frozen_string_literal: true

module Support
  module StubEnv
    def stub_env(env)
      before do
        env.each do |k, v|
          allow(ENV).to receive(:fetch).with(k) { v }
          allow(ENV).to receive(:[]).with(k) { v }
        end
      end
    end
  end
end

RSpec.configure do |conf|
  conf.extend Support::StubEnv
end
# SPDX-License-Identifier: Apache-2.0
