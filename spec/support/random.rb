# frozen_string_literal: true

require 'securerandom'

module Support
  module Random
    extend self

    def random_number(max=1_000_000_000, min: 0)
      delta = max - min
      raise ArgumentError, 'max must be greater than min' if delta < 1

      SecureRandom.rand(delta) + min
    end

    # get a random text of length in the range indicated by the `length` keyword
    # with a prefix indicated by the `prefix` keyword and restricted to the set
    # of characters specified by the `chars` keyword
    # These keyword paramters default as follows:
    # `prefix:` ''
    # `length:` 8..10
    # `chars:` UTF8 from  64 to 126, and 0x100 to 0x02ff
    def random_text(prefix: '', length: 8..10, chars: [64..126, 0x0100..0x02ff])
      len = sample(length)
      prefix + sample(chars.map(&:to_a).flatten, count: len)
               .reduce(String.new(encoding: 'utf-8')) { |r, c| r << c }
    end

    # get a random element from `from` if
    # `count` is a number get a list of random elements of
    # size `count`
    def sample(from, count: nil)
      return _single_sample(from) unless count

      (1..count)
        .map { _single_sample(from) }
    end

    private

    def _single_element(from)
      from = from.to_a
      idx = rand(from.size)
      from[idx]
    end

    def _single_sample(from)
      case from
      in Hash
        key = _single_sample(from.keys)
        [key, from[key]]
      else
        _single_element(from)
      end
    end
  end
end

RSpec.configure do |conf|
  conf.include Support::Random, random_helpers: true
  conf.extend Support::Random, random_helpers: true
end
# SPDX-License-Identifier: Apache-2.0
