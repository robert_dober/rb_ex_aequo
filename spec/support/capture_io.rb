# frozen_string_literal: true

module Support
  module CaptureIo
    def capture_puts(device: $stdout, &block)
      captured_io = []
      expect(device).to receive(:puts){ |*args| captured_io = captured_io + args }
      block.()
      captured_io
    end
  end
end
RSpec.configure do |conf|
  conf.include Support::CaptureIo, capture_io: true
end
# SPDX-License-Identifier: Apache-2.0
