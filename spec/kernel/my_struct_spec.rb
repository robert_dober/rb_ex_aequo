# frozen_string_literal: true

require 'ex_aequo/my_struct'
RSpec.describe MyStruct do

  let(:empty) { described_class.new }
  let(:cannonical) { described_class.new(a: 1, b: 2) }
  let(:complete) { described_class.from_reduce(%i[a b c d e], 0) { _1.succ } }
  let(:doubled) { described_class.from_reduce(%i[a b c d e], nil) { _2.to_s * 2 } }

  it 'can be merged into another MyStruct' do
    expect(cannonical.merge(c: 3).to_h).to eq(a: 1, b: 2, c: 3)
    expect(cannonical.merge(empty)).to eq(cannonical)
    expect(cannonical.to_h).to eq(a: 1, b: 2)
  end

  it 'can be constructed with .from_reduce' do
    expect(complete.to_h).to eq(a: 1, b: 2, c: 3, d: 4, e: 5)
    expect(doubled.to_h).to eq(a: 'aa', b: 'bb', c: 'cc', d: 'dd', e: 'ee')
  end

  it 'can see a slice' do
    expect(complete.slice(:a, :e)).to eq(described_class.new(a: 1, e: 5))
    expect(complete.slice([:a, [:b, :e]])).to eq(described_class.new(a: 1, b: 2, e: 5))
  end

  context 'fetch', :random_helpers do
    let(:default_value) { random_number }
    it 'finds a value' do
      expect(cannonical.fetch(:a)).to eq(1)
    end

    it 'can provide a default param' do
      expect(cannonical.fetch(:a, default_value)).to eq(1)
      expect(cannonical.fetch(:z, default_value)).to eq(default_value)
    end

    it 'can provide a default block' do
      expect(cannonical.fetch(:a) { default_value }).to eq(1)
      expect(cannonical.fetch(:z) { default_value }).to eq(default_value)
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
