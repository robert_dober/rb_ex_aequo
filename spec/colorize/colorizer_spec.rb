# frozen_string_literal: true

require 'ex_aequo/colorizer'
RSpec.describe ExAequo::Colorizer do

  context 'no color' do
    it 'does nothing for an empty string' do
      expect(colorize("")).to eq("")
    end
    it 'does nothing for an empty string (for some definitions of empty)' do
      expect(colorize(" ")).to eq(" ")
    end
    it 'does nothing if there is no color specified' do
      expect(colorize("hello")).to eq("hello")
    end
    it 'does nothing if NO_COLOR is specified' do
      expect(ENV).to receive(:fetch).with('NO_COLOR', false).and_return(1)
      expect(colorize('<red>red')).to eq('red')
    end
    it 'does nothing if colors are escaped' do
      expect(colorize('<<red>')).to eq('<red>')
    end
  end

  context 'colors' do
    describe 'ANSI COLORS' do
      [
        [0, :reset],
        [1, :bold],
        [2, :dim],
        [3, :italic],
        [30, :black],
        [31, :red],
        [32, :green],
        [33, :yellow],
        [34, :blue],
        [35, :magenta],
        [36, :cyan],
        [37, :white],
        [40, :bg_black],
        [41, :bg_red],
        [42, :bg_green],
        [43, :bg_yellow],
        [44, :bg_blue],
        [45, :bg_magenta],
        [46, :bg_cyan],
        [47, :bg_white]
      ].each do  |v, c|
        it "converts #{c} to \\e[#{v}m" do
          expect(colorize("<#{c}>#{c}")).to eq(ansi(v, c))
        end

      end
      it 'converts background black' do
        expect(colorize('<bg_black>')).to eq(ansi(40))
      end
    end


    describe 'reset' do
      it 'can be abbrevaited as a $' do
        expect(colorize("$")).to eq(ansi(0))
      end
      it 'can also be empty in case of NO_COLOR' do
        expect(ENV).to receive(:fetch).with('NO_COLOR', false).and_return(1)
        expect(colorize("$")).to eq('')
      end
      it 'can be esacped' do
        expect(colorize("$$")).to eq('$')
      end
      it 'can be esacped, NO_COLOR does not matter' do
        expect(ENV).to receive(:fetch).with('NO_COLOR', false).and_return(1)
        expect(colorize("$$")).to eq('$')
      end
    end

    describe 'combinations' do
      it 'returns two ANSI escapes' do
        expect(colorize('<bold,cyan>')).to eq(ansi(1, 36))
      end
    end
    describe 'some text' do
      it 'can change colors and reset' do
        expect(colorize("1<red>hello$$$<black>2")).to eq(ansi("1", 31, "hello$", 0, 30, "2"))
      end

      it 'can autoreset' do
        expect(colorize("<bold>Important", reset: true)).to eq(ansi(1, "Important", 0))
      end

      it 'autoreset observers NO_COLOR' do
        expect(ENV).to receive(:fetch).with('NO_COLOR', false).and_return(1)
        expect(colorize("<bold>Important", reset: true)).to eq("Important")
      end

      it 'can combine colors and styles' do
        input = "<bold,green>bold-green<blue,dim>blue-dim"
        expected = ansi(1, 32, 'bold-green', 34, 2, 'blue-dim')
        expect(colorize(input)).to eq(expected)
      end
    end

  end

  private
  def _ansi(ct)
    case ct
    when String, Symbol
      ct
    else
      "\e[#{ct}m"
    end
  end

  def ansi(*args)
    args.map { _ansi(_1) }.join 
  end

  def colorize(line, reset: false)
    case ExAequo::Colorizer.colorize(line, reset:)
    in {ok: true, result:}
      result
    in error
      error
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
