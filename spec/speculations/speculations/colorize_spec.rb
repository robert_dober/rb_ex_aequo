# DO NOT EDIT!!!
# This file was generated from "speculations/colorize.md" with the speculate_about gem, if you modify this file
# one of two bad things will happen
# - your documentation specs are not correct
# - your modifications will be overwritten by the speculate command line
# YOU HAVE BEEN WARNED
RSpec.describe "speculations/colorize.md" do
  # speculations/colorize.md:12
  context "Colorize" do
    # speculations/colorize.md:15
    require 'ex_aequo/colorize'
    include ExAequo::Colorize
    it "we can colorize some strings (speculations/colorize.md:21)" do
      expect(colorize('hello')).to eq(ok: true, result: 'hello')
    end
  end
end