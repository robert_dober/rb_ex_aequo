# DO NOT EDIT!!!
# This file was generated from "README.md" with the speculate_about gem, if you modify this file
# one of two bad things will happen
# - your documentation specs are not correct
# - your modifications will be overwritten by the speculate command line
# YOU HAVE BEEN WARNED
RSpec.describe "README.md" do
  # README.md:14
  context "MyStruct" do
    # README.md:23
    require 'ex_aequo/my_struct'
    let(:from_hash) { MyStruct.new(a: 1, b: 2) }
    # README.md:28
    let(:incremented) { MyStruct.from_reduce(%i[a b c d e], 0) { _1.succ } }
    it "they can be accessed like a `Hash` or `OpenStruct` (README.md:32)" do
      expect(from_hash[:a]).to eq(1)
      expect(from_hash.fetch(:b)).to eq(2)
      expect(incremented[:e]).to eq(5)
    end
    it "they are immutable (README.md:39)" do
      expect { from_hash[:c] = 3 }
      .to raise_error(MyStruct::ImmutableError, 'cannot change values with []= in immutable instance of MyStruct')
    end
    it "_mutable_ operations just create new objects (README.md:45)" do
      expect(from_hash.merge(c: 3)).to eq(MyStruct.new(a: 1, b: 2, c: 3))
      expect(from_hash.to_h).to eq(a: 1, b: 2)
    end
    it "the _hashy_ methods observer the same interface (README.md:51)" do
      expect { from_hash.fetch(:c) }
      .to raise_error(KeyError, 'key :c not found in #<MyStruct a=1, b=2> and no default given to #fetch')

      expect(from_hash.fetch(:c, 42)).to eq(42)
      expect(from_hash.fetch(:c) { 43 }).to eq(43)

      expect(incremented.slice(:a, :c, :e)).to eq(MyStruct.new(a: 1, c: 3, e: 5))
    end
  end
  # README.md:61
  context "ArgsParser and Args" do
    # README.md:67
    let(:simple_parser) do
    ExAequo::ArgsParser.new(
    allowed: %w[alpha: beta: :help :verbose],
    aliases: {h: :help, v: :verbose, a: :alpha}
    )
    end
    it ", as northing is required we can successfully parse empty arguments (README.md:77)" do
      result = simple_parser.parse([])

      expect(result).to be_ok
      expect(result.missing).to be_empty
      expect(result.superflous).to be_empty
      expect(result.positionals).to be_empty
      expect(result.keywords).to eq(MyStruct.new)
    end
    it ", we can also provide allowed values (N.B. ruby sytnax is default) (README.md:88)" do
      result = simple_parser.parse(%w[alpha: 42 43])

      expect(result).to be_ok
      expect(result.missing).to be_empty
      expect(result.superflous).to be_empty
      expect(result.positionals).to eq(%w[43])
      expect(result.keywords).to eq(MyStruct.new(alpha: '42'))
    end
  end
end