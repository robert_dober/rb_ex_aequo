[![CI](https://gitlab.com/robert_dober/rb_ex_aequo/workflows/CI/badge.svg)](https://gitlab.com/robert_dober/rb_ex_aequo/actions)
[![Coverage Status](https://coveralls.io/repos/gitlab/robert_dober/rb_ex_aequo/badge.svg?branch=master)](https://coveralls.io/gitlab/robert_dober/rb_ex_aequo?branch=master)
[![Gem Version](https://badge.fury.io/rb/ex_aequo.svg)](http://badge.fury.io/rb/ex_aequo)
[![Gem Downloads](https://img.shields.io/gem/dt/ex_aequo.svg)](https://rubygems.org/gems/ex_aequo)


# Some Tools

... which tools?

Let us [speculate about](https://github.com/RobertDober/speculate_about) that:


## Context: MyStruct

All the goodies of `OpenStruct` without the badies:

- Immutable
- Deconstructable (→ _Patternmatchable_)
- _Hashy_ Interface (`fetch`, `merge`, `slice`...)

Given an instance of `MyStruct` constructed from a `Hash`
```ruby
  require 'ex_aequo/my_struct'
  let(:from_hash) { MyStruct.new(a: 1, b: 2) }
```
And an instance constructed by a reducer
```ruby
    let(:incremented) { MyStruct.from_reduce(%i[a b c d e], 0) { _1.succ } }
```
Then they can be accessed like a `Hash` or `OpenStruct`
```ruby
    expect(from_hash[:a]).to eq(1)
    expect(from_hash.fetch(:b)).to eq(2)
    expect(incremented[:e]).to eq(5)
```

And they are immutable
```ruby
  expect { from_hash[:c] = 3 }
    .to raise_error(MyStruct::ImmutableError, 'cannot change values with []= in immutable instance of MyStruct')
```

And _mutable_ operations just create new objects
```ruby
  expect(from_hash.merge(c: 3)).to eq(MyStruct.new(a: 1, b: 2, c: 3))
  expect(from_hash.to_h).to eq(a: 1, b: 2)
```

And the _hashy_ methods observer the same interface
```ruby
    expect { from_hash.fetch(:c) }
      .to raise_error(KeyError, 'key :c not found in #<MyStruct a=1, b=2> and no default given to #fetch')

    expect(from_hash.fetch(:c, 42)).to eq(42)
    expect(from_hash.fetch(:c) { 43 }).to eq(43)

    expect(incremented.slice(:a, :c, :e)).to eq(MyStruct.new(a: 1, c: 3, e: 5))
```

## Context: ArgsParser and Args

An expressive, yet simple argument parser that takes full advantage of Ruby 3
and returns a modern Args struct (with modern I mean MyStruct) as a result.

Given a simple example like this on
```ruby
  let(:simple_parser) do
    ExAequo::ArgsParser.new(
      allowed: %w[alpha: beta: :help :verbose],
      aliases: {h: :help, v: :verbose, a: :alpha}
    )
  end
```

Then, as northing is required we can successfully parse empty arguments
```ruby
  result = simple_parser.parse([])

  expect(result).to be_ok
  expect(result.missing).to be_empty
  expect(result.superflous).to be_empty
  expect(result.positionals).to be_empty
  expect(result.keywords).to eq(MyStruct.new)
```

And, we can also provide allowed values (N.B. ruby sytnax is default)
```ruby
  result = simple_parser.parse(%w[alpha: 42 43])

  expect(result).to be_ok
  expect(result.missing).to be_empty
  expect(result.superflous).to be_empty
  expect(result.positionals).to eq(%w[43])
  expect(result.keywords).to eq(MyStruct.new(alpha: '42'))
```

## Other Tools are described [here](/speculations)

## LICENSE

Copyright 202[2,3] Robert Dober robert.dober@gmail.com

Apache-2.0 [c.f LICENSE](LICENSE)
<!-- SPDX-License-Identifier: Apache-2.0 -->
