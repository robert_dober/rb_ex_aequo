# frozen_string_literal: true

require_relative 'lib/ex_aequo/version'
version = ExAequo::Version::VERSION

description = <<~DESCRIPTION
  Some tools
DESCRIPTION

Gem::Specification.new do |s|
  s.name         = 'ex_aequo'
  s.version      = version
  s.summary      = 'Some tools'
  s.description  = description
  s.authors      = ['Robert Dober']
  s.email        = 'robert.dober@gmail.com'
  s.files        = Dir.glob('lib/**/*.rb')
  # s.files       += Dir.glob('bin/**/*')
  s.files       += %w[LICENSE README.md]
  # s.bindir       = 'bin'
  # s.executables += Dir.glob('bin/*').map { File.basename(_1) }
  s.homepage     = 'https://gitlab.com/robert_dober/rb_ex_aequo'
  s.licenses     = %w[Apache-2.0]

  s.required_ruby_version = '>= 3.3.0'

  s.add_dependency 'forwarder3', '~> 0.1.0'
  s.add_dependency 'ostruct'
end
# SPDX-License-Identifier: Apache-2.0
