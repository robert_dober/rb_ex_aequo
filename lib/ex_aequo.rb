# frozen_string_literal: true

module ExAequo
end
require 'ostruct'
require_relative 'ex_aequo/args_parser'
require_relative 'ex_aequo/version'
# SPDX-License-Identifier: Apache-2.0
