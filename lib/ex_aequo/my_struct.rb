# frozen_string_literal: true

require_relative 'my_struct/class_methods'
require 'forwarder'
class MyStruct < OpenStruct
  class ImmutableError < RuntimeError; end

  extend ClassMethods
  extend Forwarder

  # forward :fetch, to: :to_h

  def[]=(*)
    raise ImmutableError, "cannot change values with []= in immutable instance of #{self.class}"
  end

  def fetch(*args, &blk)
    to_h.fetch(*args, &blk)
  rescue KeyError
    raise KeyError,
      "key #{args.first.inspect} not found in #{self} and no default given to #fetch"
  end

  def merge(with)
    self.class.new(to_h.merge(with.to_h))
  end

  def slice(*args)
    self.class.new(to_h.slice(*args.flatten))
  end
end
# SPDX-License-Identifier: Apache-2.0
