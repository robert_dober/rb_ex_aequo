# frozen_string_literal: true

require_relative 'my_struct'

module ExAequo
  class Args

    attr_reader :missing, :positionals, :superflous

    def add_illegal_kwd(kwd)
      errors << ["Illegal kwd #{kwd}"]
    end

    def add_kwd(kwd, value)
      # Constraint Checks would go here
      @keywords[kwd.to_sym] = value
      self
    end

    def add_positional(arg)
      positionals << arg
      self
    end

    def errors
      @__errors__ ||= []
    end

    def keywords
      MyStruct.new(@keywords)
    end

    def ok?
      missing.empty? && errors.empty?
    end

    def set_flag(flag)
      @keywords[flag.to_sym] = true
      self
    end

    private

    attr_reader :aliases, :allowed, :required

    def initialize(aliases: [], allowed: [], required: [])
      @required = required
      @allowed  = allowed
      @required = required

      @bad_syntax = false
      @keywords   = {}
      @missing    = []
      @positionals = []
      @superflous = []
    end

  end
end
# SPDX-License-Identifier: Apache-2.0
