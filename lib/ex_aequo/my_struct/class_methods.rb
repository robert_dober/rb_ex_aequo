# frozen_string_literal: true

require 'ostruct'
class MyStruct < OpenStruct
  module ClassMethods
    def from_reduce(keys, acc, &reducer)
      new(**_from_reduce(keys, acc, &reducer))
    end

    private

    def _from_reduce(keys, acc, &reducer)
      keys.map do |key|
        reducer.(acc, key) => acc
        [key, acc]
      end.to_h
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
