# frozen_string_literal: true

module ExAequo
  module Tools
    module TextScanner extend self
      def scan(str, rgx: '/', ws_at_end: true)
        r = Regexp === rgx ? r : Regex.compile(rgx)
        _scan(str, rgx: r, ws_at_end:)
      end

      private
      def _scan(str, rgx:, ws_at_end:)
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
