# frozen_string_literal: true

module ExAequo
  module Version
    VERSION = '0.3.0'
  end
end
# SPDX-License-Identifier: Apache-2.0
