# frozen_string_literal: true

require_relative 'colorizer'
module ExAequo
  module Colorize
    extend Forwardable

    def_delegators Colorizer, :colorize, :colorize_file, :colorize_lines
      
    
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
