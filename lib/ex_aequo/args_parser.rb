# frozen_string_literal: true

require_relative 'args'

module ExAequo
  class ArgsParser

    FlagRgx = %r{\A : .*}x.freeze
    KwdRgx  = %r{.* : \z}x.freeze
    def parse(args)
      result = Args.new(aliases:, allowed:, required:)
      args.inject([state, result], &_parse).last
    end

    private

    attr_reader :aliases, :allowed, :posix, :required, :state

    def initialize(aliases: [], allowed: [], posix: false, required: [])
      @aliases  = aliases
      @allowed  = allowed
      @posix    = posix
      @required = required

      @state    = nil
    end

    def _parse
      ->((state, result), arg) do
        case state
        in nil
          _parse_nil(result, arg)
        in kwd
          _parse_kwd(kwd, result, arg)
        end
      end
    end

    def _parse_new_flag(flag, result)
      if allowed.include?(flag)
        [nil, result.set_flag(flag[1..-1])]
      else
        [nil, result.add_illegal_kwd(flag)]
      end
    end

    def _parse_new_kwd(kwd, result)
      if allowed.include?(kwd)
        [kwd[0...-1], result]
      else
        [nil, result.add_illegal_kwd(kwd)]
      end
    end

    def _parse_kwd(kwd, result, arg)
      [nil, result.add_kwd(kwd, arg)]
    end

    def _parse_nil(result, arg)
      case arg
      when FlagRgx
        _parse_new_flag(arg, result)
      when KwdRgx
        _parse_new_kwd(arg, result)
      else
        [nil, result.add_positional(arg)]
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
