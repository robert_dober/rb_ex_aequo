# frozen_string_literal: true

module Enumerable
  def find_value(&block)
    each do |ele|
      case block.(ele)
      in nil
        nil
      in false
        false
      in value
        return value
      end
    end
    nil
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
