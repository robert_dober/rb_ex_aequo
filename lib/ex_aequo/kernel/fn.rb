# frozen_string_literal: true

module Fn
  extend self
  def prefix_string(with)
    ->(subject) { "#{with}#{subject}" }
  end
  def split_string(by=/\s+/)
    ->(subject) {subject.split(by)}
  end
end
# SPDX-License-Identifier: Apache-2.0
