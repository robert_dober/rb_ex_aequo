# frozen_string_literal: true

module Access
  extend self

  def any_key?(hash, *keys)
    keys.flatten.reduce(false) { |_, key| return true if hash[key] }
    false
  end

  def hash_by_key(hash)
    ->(key) { hash[key] }
  end
end
# SPDX-License-Identifier: Apache-2.0
