# frozen_string_literal: true

require_relative './colorize/color_definitions'

module ExAequo
  module Colorizer extend self

    def colorize(line, reset: false)
      line = line.chomp
      no_color = ENV.fetch('NO_COLOR', false)
      case parse(line.grapheme_clusters, no_color)
      in {ok: true, result:} 
        {ok: true, result: join_result(result, no_color:, reset:)}
      in error
        error
      end
    end

    def colorize_file(file, reset: false)
      file = File.open(file, 'r') if String === file

      at_exit do
        file.close rescue nil
      end
      colorize_lines( file.readlines(chomp: true) )
    end

    def colorize_lines(lines, reset: false)
      lines = lines.split(%r{\n\r?}) if String === lines
      lines
        .each_with_index do | line, idx |
          case colorize(line, reset:)
          in {ok: true, result:}
            puts result
          in {ok: false, error:}
            $stderr.puts("ERROR in line #{idx.succ} ** #{error} **")
          end
        end
    end

    private

    def app(ary, ary_or_elem)
      case ary_or_elem
      when Array
        [*ary, *ary_or_elem]
      else
        [*ary, ary_or_elem]
      end
    end

    def check_color_definition(rest, no_color,color)
      case ColorDefinitions.get(color, no_color)
      in nil
        {ok: false, error: "undefined color #{color}"}
      in color
        {ok: true, color:}
      end
    end

    def get_color!(color, no_color)
      case ColorDefinitions.get(color, no_color)
      in nil
        raise "This should not have happened"
      in color
        color
      end
    end

    def join_result(result, no_color:, reset:)
      app(result, reset ? get_color!(:reset, no_color) : '')
        .join
    end

    def parse(line, no_color, result = [])
      # p(line:, result:)
      case line
      in [] 
        {ok: true, result:}
      in ['<', '<', *rest]
        parse(rest, no_color, app(result, '<'))
      in ['$', '$', *rest]
        parse(rest, no_color, app(result, '$'))
      in ['$', *rest]
        parse(rest, no_color, app(result, reset(no_color)))
      in ['<', *rest]
        parse_color(rest, no_color, result)
      in [char, *rest] 
        parse(rest, no_color, app(result, char))
      end
    end

    def parse_color(line, no_color, result, color=[])
      # p(line:line, result:, color:)
      case line
      in []
        {ok: false, error: "Incomplete Color Spec #{color.join}"}
      in ['>', *rest]
        parse_continue_after_color(rest, no_color, result, color.join)
      in [',', *rest]
        parse_next_color(rest, no_color, result, color.join)
      in [char, *rest]
        parse_color(rest, no_color, result, app(color, char))
      end
    end

    def parse_continue_after_color(rest, no_color, result, color)
      case check_color_definition(rest, no_color, color)
      in {ok: true, color:}
        parse(rest, no_color, app(result, color))
      in error
        error
      end
    end

    def parse_next_color(rest, no_color, result, color)
      case check_color_definition(rest, no_color, color)
      in {ok: true, color:}
        parse_color(rest, no_color, app(result, color))
      in error
        error
      end
    end

    def reset(no_color)
      get_color!(:reset, no_color)
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
