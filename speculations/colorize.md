# Colorize

The `ExAequo::Colorizer` module exposes three methods

- `colorize`
- `colorize_file`
- `colorize_lines`

However it is not really suitable to be included because of many private methods, therefore
we have the `ExAequo::Colorize` module which only exposes the three aforementioned methods

## Context: Colorize

Given we inlude `ExAequo::Colorize`
```ruby
    require 'ex_aequo/colorize'
    include ExAequo::Colorize
```

Then we can colorize some strings
```ruby
        expect(colorize('hello')).to eq(ok: true, result: 'hello')
```
<!--SPDX-License-Identifier: Apache-2.0 -->
